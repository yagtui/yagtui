# yagtui

Yet another Git TUI.

# Why?

After discovering [hydroxide](https://github.com/emersion/hydroxide), I decided to try using `git send-email` with it.

It's great. But there's a problem.

Git requires you to manually run all of the git commands you need and download email patches.

To solve this problem, yagtui provides a simple TUI that *just works*.
