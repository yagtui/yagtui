#!/usr/bin/env python3

from distutils.core import setup

setup(
	name="yagtui",
	version="0.0.1",
	description="Yet another Git TUI",
	author="hexaheximal",
	author_email="hexaheximal@proton.me",
	url="https://github.com/yagtui/yagtui",
	packages=["yagtui"]
)
