import subprocess
from . import *
import getpass
import json
import sys
import os

def main():
	inbox_was_loaded = False
	inbox_offset = 0
	git_repository = None
	
	config_path = os.path.join(os.path.join(os.environ["HOME"], ".yagtui"), "config.json")
	
	config = {
		"address": None,
		"port": None,
		"username": None,
		"password": None
	}
	
	if not os.path.isfile(config_path):
		address = input("Address: ")
		smtp_port = int(input("SMTP Port: "))
		imap_port = int(input("IMAP Port: "))
		username = input("Username: ")
		password = getpass.getpass("Password: ")
		
		config["address"] = address
		config["smtp_port"] = smtp_port
		config["imap_port"] = imap_port
		config["username"] = username
		config["password"] = password
		
		if not os.path.isdir(os.path.join(os.environ["HOME"], ".yagtui")):
			os.mkdir(os.path.join(os.environ["HOME"], ".yagtui"))
		
		f = open(config_path, "w")
		f.seek(0)
		json.dump(config, f)
		f.close()
	else:
		f = open(config_path, "r")
		config = json.load(f)
		f.close()
	
	print("Logging in...")
	
	login_imap(config["address"], config["imap_port"], config["username"], config["password"])
	
	while True:
		_command = input("> ").strip().split(" ")
		
		if len(_command) == 0:
			continue
		
		command = _command[0]
		args = _command[1:]
		
		if command == "help":
			commands = [
				"init [path] - Creates a new directory, runs git init, and sets it as the current git repository"
				"set - Sets the current git repository path",
				"get - Prints the current git repository path",
				"edit [filename] - Wrapper for nano",
				"add - Wrapper for git add",
				"commit - Wrapper for git commit",
				"log - Wrapper for git log",
				"sendp [address] - Sends a git patch to [address] (the address argument is optional though)",
				"applyp [index] - Applies an email patch to the current git repository",
				"applypf [filename] - Applies an email patch from a file",
				"export [index] [filename] - Exports an email to a file",
				"read [index] - Reads an email",
				"list - Shows messages in the inbox",
				"refresh - Refreshes the inbox",
				"next [amount] - Increases the inbox offset by [amount]",
				"back [amount] - Decreases the inbox offset by [amount]",
			]
			
			print("Commands:")
			
			for command in commands:
				print(command)
			
			continue
		
		if command == "init":
			if len(args) != 1:
				print("bad command")
				continue
			
			path = os.path.realpath(args[0])
			
			if not os.path.isdir(os.path.join(path, ".git")):
				os.mkdir(path)
				cwd = os.getcwd()
				os.chdir(path)
				os.system("git init")
				os.chdir(cwd)
				git_repository = path
				continue
			
			print("already exists")
			continue
		
		if command == "set":
			if len(args) != 1:
				print("bad command")
				continue
			
			path = os.path.realpath(args[0])
			
			if not os.path.isdir(os.path.join(path, ".git")):
				print("invalid repository")
				continue
			
			git_repository = path
			continue
		
		if command == "get":
			if git_repository == None:
				print("not set")
				continue
			
			print(git_repository)
			continue
		
		if command == "edit":
			if git_repository == None:
				print("not set")
				continue
			
			if len(args) != 1:
				print("bad command")
				continue
			
			cwd = os.getcwd()
			os.chdir(git_repository)
			
			filename = args[0]
			
			os.environ["FILENAME"] = filename
			os.system("nano ${FILENAME}")
			
			os.chdir(cwd)
			
			continue
		
		if command == "add":
			cwd = os.getcwd()
			
			os.chdir(git_repository)
			popen = subprocess.Popen(["git", "add"] + args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			popen.wait()
			
			os.chdir(cwd)
			continue
		
		if command == "commit":
			if git_repository == None:
				print("not set")
				continue
			
			cwd = os.getcwd()
			os.chdir(git_repository)
			os.system("git commit")
			os.chdir(cwd)
			continue
		
		if command == "log":
			if git_repository == None:
				print("not set")
				continue
			
			cwd = os.getcwd()
			os.chdir(git_repository)
			os.system("git log")
			os.chdir(cwd)
			continue
		
		if command == "sendp":
			if git_repository == None:
				print("not set")
				continue
			
			to = None
			
			if len(args) == 1:
				to = args[0]
			
			cwd = os.getcwd()
			os.chdir(git_repository)
			os.environ["SMTP_SERVER"] = config["address"] + ":" + str(config["smtp_port"])
			os.environ["SMTP_USERNAME"] = config["username"]
			os.environ["SMTP_PASSWORD"] = config["password"]
			
			if to != None:
				os.environ["SMTP_TO"] = to
				os.system("git send-email --smtp-server=${SMTP_SERVER} --smtp-user=${SMTP_USERNAME} --smtp-pass=${SMTP_PASSWORD} --to=${SMTP_TO} -1")
			else:
				os.system("git send-email --smtp-server=${SMTP_SERVER} --smtp-user=${SMTP_USERNAME} --smtp-pass=${SMTP_PASSWORD} -1")
			
			os.chdir(cwd)
			
			continue
		
		if command == "applyp":
			if git_repository == None:
				print("not set")
				continue
			
			if len(args) != 1 or 0 > int(args[0]) or int(args[0]) >= len(inbox):
				print("bad command")
				continue
			
			message = inbox[int(args[0])]
			
			f = open("/tmp/yagtui.eml", "w")
			f.seek(0)
			f.write(message.as_string())
			f.close()
			
			cwd = os.getcwd()
			os.chdir(git_repository)
			os.system("git am /tmp/yagtui.eml")
			os.chdir(cwd)
			os.remove("/tmp/yagtui.eml")
			
			continue
		
		if command == "applypf":
			if git_repository == None:
				print("not set")
				continue
			
			if len(args) != 1:
				print("bad command")
				continue
			
			filename = args[0]
			os.environ["PATCH_FILENAME"] = os.path.realpath(filename)
			
			cwd = os.getcwd()
			os.chdir(git_repository)
			os.system("git am ${PATCH_FILENAME}")
			os.chdir(cwd)
			
			continue
		
		if command == "export":
			if len(args) != 2:
				print("bad command")
				continue
			
			message = inbox[int(args[0])]
			path = args[1]
			
			f = open(args[1], "w")
			f.seek(0)
			f.write(message.as_string())
			f.close()
			
			print("ok")
			
			continue
		
		if command == "read":
			if not inbox_was_loaded or len(args) != 1 or 0 > int(args[0]) or int(args[0]) >= len(inbox):
				print("bad command")
				continue
			
			print(inbox[int(args[0])])
			
			continue
		
		if command == "list":
			if not inbox_was_loaded:
				refresh_inbox()
				inbox_was_loaded = True
			
			for message in inbox:
				print("From:", message["From"].replace("\r\n", ""))
				print("Subject:", message["Subject"].replace("\r\n", ""), end="\n\n")
			
			continue
		
		if command == "refresh":
			inbox_offset = 0
			refresh_inbox()
			
			if not inbox_was_loaded:
				inbox_was_loaded = True
			
			print("ok")
			
			continue
		
		if command == "next":
			if len(args) != 1:
				print("bad command")
				continue
			
			inbox_offset += int(args[0])
			
			refresh_inbox(offset=inbox_offset)
			
			for message in inbox:
				print("From:", message["From"].replace("\r\n", ""))
				print("Subject:", message["Subject"].replace("\r\n", ""), end="\n\n")
			
			continue
		
		if command == "back":
			if len(args) != 1:
				print("bad command")
				continue
			
			inbox_offset -= int(args[0])
			
			refresh_inbox(offset=inbox_offset)
			
			for message in inbox:
				print("From:", message["From"].replace("\r\n", ""))
				print("Subject:", message["Subject"].replace("\r\n", ""), end="\n\n")
			
			continue
		
		if command == "exit" or command == "quit":
			print("ok")
			break
		
		print("bad command")
	
	if inbox_was_loaded:
		logout_imap()

if __name__ == "__main__":
	main()
