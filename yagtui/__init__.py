from email.message import *
from email.parser import *
from email import policy
import imaplib
import json

smtp = None
imap = None

inbox = []

def min(a, b):
	if a > b:
		return b
	
	return a

def refresh_inbox(offset=0):
	del inbox[:]
	
	status, messages = imap.select("INBOX")
	
	if status != "OK":
		raise Exception(status)
	
	messages = int(messages[0])
	
	for _i in range(min(messages, 10)):
		i = messages - (_i + offset)
		
		if 1 > i:
			break
		
		status, thread = imap.fetch(str(i), "(RFC822)")
		
		if status != "OK":
			raise Exception(status)
		
		for response in thread:
			if isinstance(response, tuple):
				inbox.append(BytesParser(policy=policy.default).parsebytes(response[1]))

def login_imap(address, port, username, password):
	global imap
	
	imap = imaplib.IMAP4(address, port)
	imap.login(username, password)
	
	#print(imap.list())

def logout_imap():
	imap.close()
